# 环境部署：

虚拟机 CentOS 7 * 3 : Master, Slave1, Slave2

## Hadoop搭建：

[Hadoop 2.7分布式集群环境搭建](http://dblab.xmu.edu.cn/blog/1177-2/)

## Spark搭建：

[Spark 2.0分布式集群环境搭建](http://dblab.xmu.edu.cn/blog/1187-2/)

----

# Spark应用程序：WordCount

## 加载本地文件

```
./bin/spark-shell 

val textFile = sc.textFile("file:///usr/local/spark/mycode/wordcount/word.txt") // 从文件系统读取，该步为转换操作，惰性求值

textFile.first() // 该步为行动操作，触发计算

textFile.saveAsTextFile("file:///usr/local/spark/mycode/wordcount/writeback") // 导出到文件系统

val wordCount = textFile.flatMap(line => line.split(" ")).map(word => (word, 1)).reduceByKey((a, b) => a + b) // 统计词频

wordCount.collect() // 该步为行动操作，触发计算

wordCount.foreach(println) // 迭代打印
```

## 独立应用程序



[Spark2.1.0入门：第一个Spark应用程序：WordCount](http://dblab.xmu.edu.cn/blog/1311-2/)

----

# 问题拓展：

## 1. spark 和 mapreduce有哪些区别，请用具体的例子说明？

整体而言，hadoop mapreduce侧重于离线大批量计算，而spark则侧重内存以及实时计算。

Mapreduce每一个步骤发生在内存中但产生的中间值都会储存在磁盘里，下一步操作时又会将这个中间值调用到内存中，如此循环，直到最终完成。

Spark的每个步骤也是发生在内存之中但产生的中间值会直接进入下一个步骤，直到所有的步骤完成之后才会将最终结果保存进磁盘。

PS：Hadoop主要由Hadoop Distributed File System (HDFS)和Hadoop MapReduce组成，Spark可以借用Hadoop的HDFS也可独立存在。

参考：[Differences Between MapReduce And Apache Spark](https://www.educba.com/mapreduce-vs-apache-spark/)


## 2. rdd的本质是什么？

RDD(Resilient Distributed Datasets，弹性分布式数据集)是一个抽象的概念。RDD是只读的、分区记录的集合，RDD只能基于在稳定物理存储中的数据集和其他已有的RDD上执行确定性操作来创建。

Internally, each RDD is characterized by five main properties:

- A list of partitions 一组分片，即数据集的基本组成单位
- A function for computing each split 一个计算每个分片的函数
- A list of dependencies on other RDDs 对parent RDD的依赖，这个依赖描述了RDD之间的lineage
- Optionally, a Partitioner for key-value RDDs (e.g. to say that the RDD is hash-partitioned) 对于key-value的RDD，一个Partitioner
- Optionally, a list of preferred locations to compute each split on (e.g. block locations for an HDFS file) 一个列表，存储存取每个partition的preferred位置。对于一个HDFS文件来说，存储每个partition所在的块的位置。

论文：[Resilient Distributed Datasets : A Fault-Tolerant Abstraction for In-Memory Cluster Computing](https://gitlab.com/OperaRhino/spark-datawhale-8th/blob/master/papers/Resilient%20Distributed%20Datasets%20A%20Fault-Tolerant%20Abstraction%20for%20In-Memory%20Cluster%20Computing.pdf)

