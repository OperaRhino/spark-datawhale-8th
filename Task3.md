# DataFrame和RDD的区别

## DataFrame比RDD多了schema

RDD是分布式的 Java对象的集合，Java类的内部结构对于RDD而言却是不可知的。

DataFrame是一种以RDD为基础的分布式数据集，也就是分布式的Row对象的集合（每个Row对象代表一行记录），提供了详细的结构信息，也就是我们经常说的模式（schema），
Spark SQL可以清楚地知道该数据集中包含哪些列、每列的名称和类型。

## DataFrame采用惰性机制

DataFrame的各种变换操作同RDD一样也采用惰性机制，只是记录了各种转换的逻辑转换路线图（是一个DAG图），不会发生真正的计算，这个DAG图相当于一个逻辑查询计划，最终，会被翻译成物理查询计划，生成RDD DAG，按照之前介绍的RDD DAG的执行方式去完成最终的计算得到结果。

参考：[Spark入门：DataFrame与RDD的区别(Python版)](http://dblab.xmu.edu.cn/blog/1718-2/)


# DataFrame的创建

[Spark2.1.0入门：DataFrame的创建(Python版)](http://dblab.xmu.edu.cn/blog/1719-2/)

[Spark2.1.0+入门：从RDD转换得到DataFrame(Python版)](http://dblab.xmu.edu.cn/blog/1724-2/)

[Spark2.1.0入门：通过JDBC连接数据库(DataFrame)(Python版)](http://dblab.xmu.edu.cn/blog/1724-2/)

# spark sql的运行机制

Spark SQL的核心是一个叫做Catalyst的查询编译器，它将用户程序中的SQL/Dataset/DataFrame经过一系列操作，最终转化为Spark系统中执行的RDD。

Catalyst中的框架如下图所示，它有以下几个重要组成部分：

- Parser

兼容ANSI SQL 2003标准和HiveQL。将SQL/Dataset/DataFrame转化成一棵未经解析（Unresolved）的树，在Spark中称为逻辑计划（Logical Plan），它是用户程序的一种抽象。

- Analyzer

利用目录（Catalog）中的信息，对Parser中生成的树进行解析。Analyzer有一系列规则（Rule）组成，每个规则负责某项检查或者转换操作，如解析SQL中的表名、列名，同时判断它们是否存在。通过Analyzer，我们可以得到解析后的逻辑计划。

- Optimizer

对解析完的逻辑计划进行树结构的优化，以获得更高的执行效率。优化过程也是通过一系列的规则来完成，常用的规则如谓词下推（Predicate Pushdown）、列裁剪（Column Pruning）、连接重排序（Join Reordering）等。此外，Spark SQL中还有一个基于成本的优化器（Cost-based Optmizer），是由DLI内部开发并贡献给开源社区的重要组件。该优化器可以基于数据分布情况，自动生成最优的计划。

- Planner

将优化后的逻辑计划转化成物理执行计划（Physical Plan）。由一系列的策略（Strategy）组成，每个策略将某个逻辑算子转化成对应的物理执行算子，并最终变成RDD的具体操作。注意在转化过程中，一个逻辑算子可能对应多个物理算子的实现，如join可以实现成SortMergeJoin或者BroadcastHashJoin，这时候需要基于成本模型（Cost Model）来选择较优的算子。上面提到的基于成本的优化器在这个选择过程中也能起到关键的作用。

经过上述的一整个流程，就完成了从用户编写的SQL语句（或DataFrame/Dataset），到Spark内部RDD的具体操作逻辑的转化。

参考：[一文读懂Spark SQL运行流程](https://zhuanlan.zhihu.com/p/58428916)

