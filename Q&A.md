#### hadoop在计算方面有什么优于spark的吗？

spark劣势:
- 稳定性方面，由于代码质量问题，Spark长时间运行会经常出错。（目前版本是否仍是如此不确定）
- 在架构方面，由于大量数据被缓存在RAM中，Java回收垃圾缓慢的情况严重，导致Spark性能不稳定，在复杂场景中SQL的性能甚至不如现有的Map/Reduce。
- 不能处理单机"大数据"，单独机器处理数据过大，或者由于数据出现问题导致中间结果超过RAM的大小时，常常出现RAM空间不足或无法得出结果。然而，Map/Reduce运算框架可以处理大数据，在这方面，Spark不如Map/Reduce运算框架有效。

#### spark避免了磁盘的读写，但对内存使用不节制，导致job挂掉，有什么方法避免吗？

#### Spark和hadoop真实工作环境中是否需要一起运用？

不一定，spark可以使用hadoop提供hdfs分布式文件系统，也可以用其他的。

#### rank如何加入到rdd中的时，如何添加值相等，排名相等的条件？

#### 有没有直接用RDD算子就能直接解决排名的方法？

#### groupBy和groupByKey的区别？

#### 共享变量，广播的应用可以理解，累加器的应用是什么？应用场景是什么样子的？

#### 持久化是将数据载入到内存嘛？如果有很大量的数据作为中间结果需要怎样操作能提升计算速度呢？

#### Hive on Spark和Spark SQl区别？

#### 什么时候使用sparkSql？什么时候使用RDD？他们的效率有什么差别？

#### 什么时候使用DataFrame的API比较好？

- 需要丰富的语义，high-level抽象，和特定领域语言API，那你可DataFrame或者Dataset；
- 半结构化数据集需要high-level表达， filter，map，aggregation，average，sum ，SQL 查询，- 列式访问和使用lambda函数，那你可DataFrame或者Dataset；
- 需要编译时高度的type-safety，Catalyst优化和Tungsten的code生成，那你可DataFrame或者Dataset；
- 需要统一和简化API使用跨Spark的Library，那你可DataFrame或者Dataset；

#### Spark与其他流计算框架有什么区别，最大的优势在哪里？

#### 为什么要把rdd转化成dataframe的形式进行计算？

在spark中。dataframe类似于传统数据库中的二维表格，dataframe带有schema信息，可以直接针对hdfs等任何可以构建为RDD的数据，可以通过spark sql查看更多的结构信息

#### 对于tensorflow和pytorch这类工具对于大量数据可以使用分批加载数据的方式进行训练，那么spark.ml相对于这两种工具的优势在哪？

#### 用spark做机器学习有什么优势吗？

- Spark ml参照sklearn pipline设计的，上手更容易
- Spark提供了大数据处理能力
- Spark生态也很好，主流的模型都有lgb xgb，甚至是nn

#### 如何把神器xgboost放在spark上运行？

- Xgb spark版
- H2o spark版，内置xgb
- Python/java/scala训练好的模型spark也能直接加载

#### ml 和 mllib 两个包之间相同的模块，他们的代码原理是一样的，怎么会有格式不兼容，是有什么区别吗，例如Vector和Vectors

spark.mllib包含基于RDD的原始算法API。在1.0 以前的版本即已经包含了，提供的算法实现都是基于原始的 RDD，3.0以后会移除。

spark.ml 则提供了基于DataFrames 高层次的API，可以用来构建机器学习工作流（PipeLine）。ML Pipeline 弥补了原始 MLlib 库的不足，向用户提供了一个基于 DataFrame 的机器学习工作流式 API 套件。

所以新的代码应尽量采用ml库。

#### spark和tensorflow相比用来做机器学习有什么优势？