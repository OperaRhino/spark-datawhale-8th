# 从文件系统中加载数据创建RDD - 词频统计

```
./bin/spark-shell 

val textFile = sc.textFile("file:///usr/local/spark/mycode/wordcount/word.txt") // 从文件系统读取，该步为转换操作，惰性求值

textFile.first() // 该步为行动操作，触发计算

val wordCount = textFile.flatMap(line => line.split(" ")).map(word => (word, 1)).reduceByKey((a, b) => a + b) // 统计词频

wordCount.collect() // 该步为行动操作，触发计算

wordCount.foreach(println) // 迭代打印
```
[Spark2.1.0入门：RDD编程](http://dblab.xmu.edu.cn/blog/1312-2/)

# 通过并行集合创建RDD - 销量均值

```
val rdd = sc.parallelize(Array(("spark",2),("hadoop",6),("hadoop",4),("spark",6)))

rdd.mapValues(x => (x, 1)).reduceByKey((x, y) => (x._1+y._1,x._2+y._2)).mapValues(x => (x._1/x._2)).collect()

```
[Spark入门：键值对RDD(Python版)](http://dblab.xmu.edu.cn/blog/1706-2/)

# 文件数据读写 - JSON

```
val jsonStr = sc.textFile("file:///usr/local/spark/examples/src/main/resources/people.json")

jsonStr.foreach(print)

```

[Spark2.1.0+入门：文件数据读写(Python版)](http://dblab.xmu.edu.cn/blog/1708-2/)

# PTA排名器

```
00002 2 12
00007 4 17
00005 1 19
00007 2 25
00005 1 20
00002 2 2
00005 1 15
00001 1 18
00004 3 25
00002 2 25
00005 3 22
00006 4 -1
00001 2 18
00002 1 20
00004 1 15
00002 4 18
00001 3 4
00001 4 2
00005 2 -1
00004 2 0
```

题目：[PAT Judge](https://pintia.cn/problem-sets/16/problems/677)

```
val tuple = pta.map(line => line.split(" ")).map(x => ((x(0),x(1).toInt),x(2).toInt))
val keepmax = tuple.reduceByKey((x, y) => if (x>y){x} else {y})

val formerge = keepmax.map(x => (x._1._1, (x._1._2, x._2)))

val formerge1 = formerge.map(x => if (x._2._2 > 0) {(x._1, x._2._2)} else{(x._1, 0)})

formerge1.reduceByKey((a, b) => a + b).sortBy(_._2,false).collect()

```
